import { connect } from 'react-redux';
import Figures from './Figures';

import { getFigures } from '../../actions/figures';
import { selectFigure } from '../../actions/appState';

const mapStateToProps = state => ({
    appState: state.appState,
    figures: state.figures
});

const mapDispatchToProps = dispatch => ({
    onGetFigures: (metal, type) => {
        dispatch(getFigures(metal, type))
    },
    onSelectFigure: (figure) => {
        dispatch(selectFigure(figure));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Figures)