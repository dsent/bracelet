import React from 'react';
import './Figures.css';

class Figures extends React.Component {
    selectFigure(figure) {
        this.props.onSelectFigure(figure);
    }

    render() {
        return (
            <div className="Figures">
                <div className="Figures__title">
                    Выберите фигурку, которую хотите добавить на браслет
                </div>

                <div className="Figures-list">
                    {this.props.figures.map(item =>
                        <div className="Figures-list__item" key={item.id} onClick={() => this.selectFigure(item)}>
                            <img src={item.imagePreview} alt=""/>
                            <div dangerouslySetInnerHTML={{__html: item.description}} />
                            <div><span dangerouslySetInnerHTML={{__html: item.price}} /> руб.</div>
                            <button>Добавить</button>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default Figures;