import { connect } from 'react-redux';
import Order from './Order';

const mapStateToProps = state => ({
    appState: state.appState,
});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Order)