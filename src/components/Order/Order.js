import React, { Component } from 'react';
import axios from 'axios';
import Swal from "sweetalert2";

import './Order.css';

export default class Order extends Component {
    constructor(props) {
        super(props);

        this.userData = {
            fname: React.createRef(),
            lname: React.createRef(),
            phone: React.createRef(),
            email: React.createRef(),
            comment: React.createRef(),
        }
    }

    get metalName() {
        switch (this.props.appState.metal) {
            case 'silver':
                return 'Серебро';

            case 'gilding':
                return 'Позолота';

            case 'gold':
                return 'Золото';

            case 'wgold':
                return 'Белое золото';

            case 'rgold':
                return 'Красное золото';

            default:
                return '';

        }
    }

    sendData() {
        const data = {
            ...this.props.appState,
            userData: {
                fname: this.userData.fname.current.value,
                lname: this.userData.lname.current.value,
                phone: this.userData.phone.current.value,
                email: this.userData.email.current.value,
                comment: this.userData.comment.current.value,
            }
        }
        axios
            .post('/api/bracelets/send.php', data)
            .then(function(response) {
                if(response.data) {
                    Swal.fire({
                        type: 'success',
                        title: 'Заказ принят',
                        text: 'Наши менеджеры свяжутся с Вами'
                      }).then(function() {
                        window.location.reload();
                      })
                }
            })
    }

    render() {
        const figuresList = [];
        const namesList = [];

        this.props.appState.braceletItems.forEach(item => {
            if(item.price) {
                figuresList.push(item);
            } else {
                namesList.push(item);
            }
        });

        return (
            <div className="Order">
                <div className="Order__title">Состав заказа</div>

                <div className="Order-composition">
                    <div className="row">
                        <div className="col-md-4 mb-3"><b>Металл:</b></div>
                        <div className="col-md-8 mb-3">{ this.metalName }</div>

                        <div className="col-md-4 mb-3"><b>Цепочка:</b></div>
                        <div className="col-md-8 mb-3">{ this.props.appState.chain.text }</div>

                        <div className="col-md-4 mb-3"><b>Родиевое покрытие:</b></div>
                        <div className="col-md-8 mb-3">{ this.props.appState.rhodium ? 'Есть' : 'Нет' }</div>

                        <div className="col-md-4 mb-3"><b>Позолота:</b></div>
                        <div className="col-md-8 mb-3">{ this.props.appState.gilding ? 'Есть' : 'Нет' }</div>

                        <div className="col-md-4 mb-3"><b>Фигурки:</b></div>
                        <div className="col-md-8 mb-3">
                            { figuresList.map(item => {
                                return (
                                    <div className="Order-composition__figure" key={ item.identify }>
                                        <img src={ item.imagePreview } alt=""/>
                                        <div>
                                            <div>Цена: { item.price } руб. </div>
                                            <div>Камни: { item.stone ? item.stone.name : 'Нет' }</div>
                                            <div>Гравировка: { item.engraving || 'Нет' }</div>
                                        </div>
                                    </div>
                                )
                            }) }
                        </div>

                        <div className="col-md-4 mb-3"><b>Имена:</b></div>
                        <div className="col-md-8 mb-3">
                            { namesList.map(item => {
                                return (
                                    <div className="Order-composition__figure" key={ item.identify }>
                                        <span style={{fontFamily: item.font, fontSize: 50}}>{ item.text }</span>
                                    </div>
                                )
                            }) }
                        </div>
                    </div>
                </div>

                <div className="Order__title">Введите свои контактные данные</div>

                <div className="row">
                    <div className="col-md-6">
                        <label>Имя</label>
                        <input ref={this.userData.fname} type="text" name="fname"/>
                    </div>
                    <div className="col-md-6">
                        <label>Фамилия</label>
                        <input ref={this.userData.lname} type="text" name="lname"/>
                    </div>
                    <div className="col-md-6">
                        <label>Телефон</label>
                        <input ref={this.userData.phone} type="text" name="phone"/>

                        <label>E-mail</label>
                        <input ref={this.userData.email} type="text" name="email"/>
                    </div>
                    <div className="col-md-6">
                        <label>Комментарий</label>
                        <textarea ref={this.userData.comment} name="comment" />
                    </div>

                    <div className="col-md-12">
                        <button onClick={() => this.sendData()}>Отправить заказ</button>
                    </div>
                </div>
            </div>
        )
    }
}