import { connect } from 'react-redux';
import EditFigure from './EditFigure';

import { editFigure } from '../../actions/appState';

const mapStateToProps = state => ({
    appState: state.appState
});

const mapDispatchToProps = dispatch => ({
    onEditFigure: (figureList) => {
        dispatch(editFigure(figureList))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(EditFigure)