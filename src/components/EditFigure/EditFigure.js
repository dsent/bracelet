import React, { Fragment } from 'react';
import './EditFigure.css';

import classNames from 'classnames';
import _ from 'lodash';

class EditFigure extends React.Component {
    constructor(props) {
        super(props);
        this.grav = React.createRef();
        this.state = {
            selectStone: this.props.appState.figureViewing.stone,
            stoneList: [
                { name: "Белый", image: "/static/images/stones/1.png" },
                { name: "Черный", image: "/static/images/stones/2.png" },
                { name: "Синий", image: "/static/images/stones/3.png" },
                { name: "Голубой", image: "/static/images/stones/4.png" },
                { name: "Желтый", image: "/static/images/stones/5.png" },
                { name: "Зеленый", image: "/static/images/stones/6.png" },
                { name: "Розовый", image: "/static/images/stones/7.png" },
                { name: "Гранат", image: "/static/images/stones/9.png" },
                { name: "Рубин", image: "/static/images/stones/10.png" },
            ]
        };
    }

    componentDidMount() {
        if(this.grav.current) {
            this.grav.current.value = this.props.appState.figureViewing.engraving;
        }
    }

    selectStone(stone) {
        this.setState({ selectStone: stone});
    }

    gravsRender() {
        return (
            <Fragment>
                <input type="text" ref={this.grav} placeholder="* Укажите текст гравировки"/>

                <p>* На каждом кулоне кроме дополнительных фигурок вы можете заказать гравировку: например имя, дата рождения, вес, рост малыша. Также можно написать популярные слова: Люблю, Любимой, Маме, и др. Шрифт для гравировки используется стандартный печатными буквами формата Arial.</p>
            </Fragment>
        );
    }

    stonesRender() {
        return <div className="AddFigure__stones">
            {this.state.stoneList.map(item =>
                <label
                    key={item.name}
                    className={classNames(
                        {'active': this.state.selectStone && (item.name === this.state.selectStone.name)}
                    )}
                    onClick={() => this.selectStone(item)}
                >
                    <img src={item.image} alt=""/>
                    <span>{item.name}</span>
                    <div className="stone-checkbox" />
                </label>
            )}
        </div>
    }

    editFigure(figure) {
        const figureClone = _.clone(figure);
        figureClone.stone = this.state.selectStone;
        figureClone.engraving = this.grav.current.value;

        let figureList = this.props.appState.braceletItems.map(item => {
            if(item.identify === figureClone.identify) {
                item = figureClone;
            }

            return item;
        });

        this.props.onEditFigure(figureList);
    }

    render() {
        return (
            <div className="AddFigure">
                <div className="AddFigure__col">
                    <div className="AddFigure__image">
                        <img src={this.props.appState.figureViewing.imagePreview} alt=""/>
                    </div>
                </div>

                <div className="AddFigure__col">
                    <h3 dangerouslySetInnerHTML={{__html: this.props.appState.figureViewing.description}} />
                    { this.props.appState.figureViewing.gravs && this.gravsRender() }
                </div>

                { this.props.appState.figureViewing.stones && this.stonesRender() }

                <div className={classNames(
                    "AddFigure__price-add",
                    { "AddFigure__price-add--full": !this.props.appState.figureViewing.gravs }
                )}>
                    <div className="AddFigure__price">
                        { this.props.appState.figureViewing.price } руб.
                    </div>
                    <div className="AddFigure__add">
                        <button onClick={() => this.editFigure(this.props.appState.figureViewing)}>Изменить</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default EditFigure;