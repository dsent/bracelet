import React from 'react';
import classNames from 'classnames';

class Metals extends React.Component {
    componentDidMount() {
        this.props.onGetMetals();
    };

    changeMetal(code) {
        this.props.onChangeMetal(code);

        if(this.props.appState.selectAddType) {
            this.props.onGetFigures(code, this.props.appState.selectAddType);
        }

        if(this.props.appState.selectAddType === 'ordering') {
            this.props.onChangeAddType(null);
        }
    }

    render() {
        return (
            <div className="Metals">
                <div className="Sidebar__title">МЕТАЛЛ</div>

                <div className="checkboxes-list">
                    {this.props.metals.map(metal => (
                        <div
                            className={
                                classNames(
                                    'checkboxes-list__item',
                                    { 'checkboxes-list__item--active': (metal.code === this.props.appState.metal) },
                                    { 'checkboxes-list__item--active': (metal.code === 'silver' && this.props.appState.metal === 'gilding') }
                                )
                            }
                            key={ metal.id }>
                            <div className="checkbox-block" onClick={() => this.changeMetal(metal.code)}>
                                <div className="checkbox-block__custom" />
                                <div className="checkbox-block__text">{ metal.name }</div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default Metals;