import { connect } from 'react-redux';
import Metals from './Metals';

import { getMetals } from '../../../actions/metals';
import { getChains } from '../../../actions/chains';
import { getFigures } from '../../../actions/figures';
import { changeMetal, changeAddType } from '../../../actions/appState';

const mapStateToProps = state => ({
    appState: state.appState,
    metals: state.metals
});

const mapDispatchToProps = dispatch => ({
    onGetMetals: () => {
        dispatch(getMetals())
    },
    onChangeMetal: (code) => {
        dispatch(changeMetal(code));
        dispatch(getChains(code))
    },
    onGetFigures: (metal, type) => {
        dispatch(getFigures(metal, type))
    },
    onChangeAddType(code) {
        dispatch(changeAddType(code));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Metals)