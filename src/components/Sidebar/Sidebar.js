import React from 'react';
import './Sidebar.css';

import Metals from './Metals';
import Chains from './Chains';
import AddItem from './AddItem';

const Sidebar = () => {
    return (
        <div className="Sidebar">
            <div className="Sidebar__container"><Metals /></div>
            <div className="Sidebar__container"><Chains /></div>
            <div className="Sidebar__container"><AddItem /></div>
            <img style={{ maxWidth: "100%" }} src="http://www.namenecklace.ru/generator/images/imgpsh_fullsize.png" alt="" />
        </div>
    )
};

export default Sidebar;