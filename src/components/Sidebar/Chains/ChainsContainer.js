import { connect } from 'react-redux';
import Chains from './Chains';

import { getChains } from '../../../actions/chains';
import { changeChain } from '../../../actions/appState';

const mapStateToProps = state => ({
    appState: state.appState,
    chains: state.chains
});

const mapDispatchToProps = dispatch => ({
    onGetChains: (metal) => {
        dispatch(getChains(metal));
    },
    onChangeChain: (chain) => {
        dispatch(changeChain(chain));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Chains)