import React from 'react';
import classNames from "classnames";

class Chains extends React.Component {
    componentDidMount() {
        this.props.onGetChains(this.props.appState.metal);
    };

    changeChain(chain) {
        this.props.onChangeChain(chain);
    }

    render() {
        return (
            <div className="Chains">
                <div className="Sidebar__title">ЦЕПОЧКА</div>

                <div className="checkboxes-list">
                    {this.props.chains.map(chain => (
                        <div
                            className={
                                classNames(
                                    'checkboxes-list__item',
                                    { 'checkboxes-list__item--active': (chain.value === this.props.appState.chain.value) }
                                )
                            }
                            key={ chain.id }>
                            <div className="checkbox-block" onClick={() => this.changeChain(chain)}>
                                <div className="checkbox-block__custom" />
                                <div className="checkbox-block__text">{ chain.text }</div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default Chains;