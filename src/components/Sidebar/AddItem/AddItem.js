import React from 'react';
import classNames from 'classnames';

class AddItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectAddType: [
                {
                    'name': 'ДОБАВИТЬ ИМЯ',
                    'key': 'name'
                },
                {
                    'name': 'МАЛЬЧИК',
                    'key': 'boys'
                },
                {
                    'name': 'ДЕВОЧКА',
                    'key': 'girls'
                },
                {
                    'name': 'ДОПОЛНИТЕЛЬНЫЕ',
                    'key': 'pendants'
                }
            ]
        }
    }

    changeAddType(key) {
        this.props.onChangeAddType(this.props.appState.metal, key);
    }

    render() {
        return (
            <div className="AddItem">
                <div className="Sidebar__title">ФИГУРКИ И ИМЕНА</div>

                <div className="checkboxes-list">
                    {this.state.selectAddType.map(addType => (
                        <div
                            className={
                                classNames(
                                    'checkboxes-list__item',
                                    { 'checkboxes-list__item--active': (addType.key === this.props.appState.selectAddType) }
                                )
                            }
                            key={ addType.key }>
                            <div className="checkbox-block" onClick={() => this.changeAddType(addType.key)}>
                                <div className="checkbox-block__custom" />
                                <div className="checkbox-block__text">{ addType.name }</div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default AddItem;