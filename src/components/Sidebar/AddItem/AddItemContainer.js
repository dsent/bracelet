import { connect } from 'react-redux';
import AddItem from './AddItem';

import { changeAddType } from '../../../actions/appState';
import { getFigures } from '../../../actions/figures';

const mapStateToProps = state => ({
    appState: state.appState
});

const mapDispatchToProps = dispatch => ({
    onChangeAddType: (metal, key) => {
        dispatch(changeAddType(key));
        dispatch(getFigures(metal, key));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AddItem)