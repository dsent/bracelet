import { connect } from 'react-redux'
import App from './App'

import { changeAddType } from "../../actions/appState";
import { getFigures } from '../../actions/figures';

const mapStateToProps = state => ({
    appState: state.appState,
    chains: state.chains
});

const mapDispatchToProps = dispatch => ({
    onChangeAddType(code) {
        dispatch(changeAddType(code));
    },
    onFirstLoad(metal, type) {
        dispatch(getFigures(metal, type));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(App)