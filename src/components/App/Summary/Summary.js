import React from 'react';

const textPrice = (text, metal) => {
    let fl, rl;
    switch (metal) {
        case "silver":
            fl = 2500;
            rl = 200;
            break;

        case "gilding":
            fl = 4000;
            rl = 200;
            break;

        case "gold":
            fl = 6500;
            rl = 500;
            break;

        case "wgold":
            fl = 7100;
            rl = 500;
            break;

        case "rgold":
            fl = 7100;
            rl = 500;
            break;

        default:
            fl = 0;
            rl = 0;
            break;
    }

    if (text.length) {
        return fl + rl*(text.length - 1);
    } else {
        return 0;
    }
};

const Summary = (props) => {
    let figuresList = [];
    let namesList = [];
    let sum = 0;

    props.data.figures.forEach(item => {
        if(item.price) {
            figuresList.push(item);
        } else {
            namesList.push(item);
        }
    });

    sum += props.data.chain ? +props.data.chain.price : 0;

    sum += figuresList.reduce((sum, current) => {
        return sum + parseInt(current.price);
    }, 0);

    sum += namesList.reduce((sum, current) => {
        return sum + textPrice(current.text, props.data.metal);
    }, 0);

    sum += props.data.rhodium ? 600 : 0;

    sum += props.data.gilding ? 1000 : 0;

    return (
        <div className="App-ordering__price">
            ЦЕНА:
            <span>{sum} руб.</span>
        </div>
    )
};

export default Summary;