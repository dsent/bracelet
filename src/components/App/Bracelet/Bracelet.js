import React from 'react';
import classNames from "classnames";
import _ from 'lodash';

import './Bracelet.css';

class Bracelet extends React.Component {
    moveItem(item, direction) {
        let braceletItems = _.clone(this.props.appState.braceletItems);
        let index = braceletItems.indexOf(item);
        braceletItems.splice(index, 1);
        braceletItems.splice(direction === 'left' ? --index : ++index, 0, item);
        this.props.onChangeFiguresList(braceletItems);
    }

    removeItem(item) {
        let braceletItems = _.clone(this.props.appState.braceletItems);

        _.remove(braceletItems, bi => {
            return bi === item;
        });

        this.props.onRemoveFigure(braceletItems);
    }

    editItem(item) {
        this.props.onEditFigure('edit', item);
    }

    renderItem(item) {
        if(item.id) {
            return <img src={ item.imageArea } alt=""/>;
        } else {
            const style = {
                fontFamily: item.font,
                color: _.find(this.props.metals, { code: this.props.appState.metal }).color
            };

            return <span style={style}>{ item.text }</span>;
        }
    }

    render() {
        const chainImg = this.props.appState.chain && `url('${this.props.appState.chain.image}')`;

        return (
            <div className={
                classNames(
                    'App-bracelet',
                    { 'App-bracelet--silver': !this.props.appState.braceletItems.length && !this.props.appState.chain && (this.props.appState.metal === 'silver'  || this.props.appState.metal === 'wgold') },
                    { 'App-bracelet--gold': !this.props.appState.braceletItems.length && !this.props.appState.chain && (this.props.appState.metal === 'gold'  || this.props.appState.metal === 'gilding') },
                    { 'App-bracelet--rgold': !this.props.appState.braceletItems.length && !this.props.appState.chain && this.props.appState.metal === 'rgold' }
                )}
                 style={{"backgroundImage": chainImg}}
            >
                {this.props.appState.braceletItems.map((item, i) =>
                    <div className="App-bracelet__item" key={item.identify}>

                        { this.renderItem(item) }

                        { i !== 0 && <button className="App-bracelet__left" onClick={() => this.moveItem(item, 'left')} /> }
                        { i !== this.props.appState.braceletItems.length-1 && <button className="App-bracelet__right" onClick={() => this.moveItem(item, 'right')} /> }

                        <div className="App-bracelet__popover">
                            { item.id && <button onClick={() => this.editItem(item)}>Изменить</button> }
                            <button onClick={() => this.removeItem(item)}>Удалить</button>
                        </div>
                    </div>
                )}
            </div>
        )
    }
}

export default Bracelet;