import { connect } from 'react-redux'
import Bracelet from './Bracelet'

import { changeFiguresList, openEditFigure, removeFigure } from '../../../actions/appState';

const mapStateToProps = state => ({
    appState: state.appState,
    metals: state.metals
});

const mapDispatchToProps = dispatch => ({
    onChangeFiguresList: figuresList => {
        dispatch(changeFiguresList(figuresList));
    },
    onRemoveFigure: figuresList => {
        dispatch(removeFigure(figuresList));
    },
    onEditFigure: (type, figure) => {
        dispatch(openEditFigure(type, figure));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Bracelet)