import { connect } from 'react-redux'
import SilverSettings from './SilverSettings'

import { changeGilding, changeRhodium } from '../../../actions/appState';
import { getFigures } from "../../../actions/figures";

const mapStateToProps = state => ({
    appState: state.appState
});

const mapDispatchToProps = dispatch => ({
    onChangeGilding: () => {
        dispatch(changeGilding());
    },

    onChangeRhodium: () => {
        dispatch(changeRhodium());
    },

    onGetFigures: (metal, type) => {
        dispatch(getFigures(metal, type))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SilverSettings)