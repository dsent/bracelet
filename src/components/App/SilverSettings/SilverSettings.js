import React, { Component } from 'react';
import './SilverSettings.css';
import classNames from "classnames";

export default class SilverSettings extends Component{
    changeMetal(code) {
        this.props.onChangeMetal(code);

        if(this.props.appState.selectAddType) {
            this.props.onGetFigures(code, this.props.appState.selectAddType);
        }
    }

    changeRhodium() {
        this.props.onChangeRhodium();
    }

    changeGilding() {
        this.props.onChangeGilding();
    }

    renderRhodium() {
        if(this.props.appState.metal === 'silver' && !this.props.appState.gilding) {
            return (
                <div
                    className={
                        classNames(
                            'checkboxes-list__item',
                            { 'checkboxes-list__item--active': this.props.appState.rhodium }
                        )
                    }>
                    <div className="checkbox-block" onClick={() => this.changeRhodium()}>
                        <div className="checkbox-block__custom" />
                        <div className="checkbox-block__text">Родий</div>
                    </div>
                </div>
            )
        }
    }

    renderGilding() {
        if(this.props.appState.metal === 'silver' && !this.props.appState.rhodium) {
            return (
                <div
                    className={
                        classNames(
                            'checkboxes-list__item',
                            { 'checkboxes-list__item--active': this.props.appState.gilding }
                        )
                    }>
                    <div className="checkbox-block" onClick={() => this.changeGilding()}>
                        <div className="checkbox-block__custom" />
                        <div className="checkbox-block__text">Позолота</div>
                    </div>
                </div>
            )
        }
    }

    render() {
            return (
                <div className="SilverSettings">
                    { this.renderRhodium() }
                    { this.renderGilding() }
                </div>
            );
    }
}