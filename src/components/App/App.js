import React, { Component } from 'react';
import '../../assets/fonts/fonts.css';

import './App.css';

import Summary from "./Summary";
import Bracelet from "./Bracelet";
import SilverSettings from "./SilverSettings";

import Sidebar from "../Sidebar";
import Welcome from "../Welcome";

import Figures from "../Figures";
import AddFigure from "../AddFigure";
import EditFigure from "../EditFigure";
import AddText from "../AddText";
import Order from '../Order';

class App extends Component {
    selectAddType() {
        if(!this.props.appState.selectAddType) {
            return <Welcome />;
        } else if(this.props.appState.selectAddType === 'name') {
            return <AddText />;
        } else if(this.props.appState.selectAddType === 'edit') {
            return <EditFigure />;
        } else if(this.props.appState.selectAddType === 'ordering') {
            return <Order />;
        } else if(this.props.appState.selectAddType && !this.props.appState.figureViewing) {
            return <Figures />
        } else {
            return <AddFigure />
        }
    }

    changeAddType(code) {
        this.props.onChangeAddType(code);
    }

    componentDidMount() {
        this.props.onChangeAddType('boys');
        this.props.onFirstLoad('silver', 'boys');
    }

  render() {
    const summaryData = {
        chain: this.props.appState.chain,
        figures: this.props.appState.braceletItems,
        metal: this.props.appState.metal,
        rhodium: this.props.appState.rhodium,
        gilding: this.props.appState.gilding
    };

    return (
      <div className="App">
        <div className="App-settings">
            <Sidebar />
            <div className="App-settings__select-items">
                {this.selectAddType() }
            </div>
        </div>

        <Bracelet />

        <div className="App-ordering">
            <Summary data={summaryData} />

            <SilverSettings />

            <div className="App-ordering__submit">
                <button disabled={!this.props.appState.braceletItems.length || !this.props.appState.chain} onClick={() => this.changeAddType('ordering')}>ЗАКАЗАТЬ</button>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
