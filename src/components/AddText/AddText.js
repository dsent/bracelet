import React from 'react';
import classNames from 'classnames';
import './AddText.css';

class AddText extends React.Component {

    constructor(props) {
        super(props);

        this.text = React.createRef();

        this.state = {
            en: [
                {name: "daythk", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/0.png`},
                {name: "angliascriptconcise", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/1.png`},
                {name: "Aquarelle", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/2.png`},
                {name: "ballantines_regular", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/3.png`},
                {name: "Brawler", size: "45", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/4.png`},
                {name: "BRUSHSCN", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/5.png`},
                {name: "candice", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/6.png`},
                {name: "comic", size: "45", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/7.png`},
                {name: "comicbd", size: "45", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/8.png`},
                {name: "commercial script",size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/9.png`},
                {name: "engroleb", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/10.png`},
                {name: "esenin_script_two", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/11.png`},
                {name: "harrington", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/12.png`},
                {name: "LaurenScript_2", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/13.png`},
                {name: "MTCORSVA", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/14.png`},
                {name: "Ribbon Heart", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/16.png`},
                {name: "SCRIPTBL", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/17.png`},
                {name: "verdanab", size: "45", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/20.png`},
                {name: "verdanaz", size: "45", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/21.png`}
            ],
            ru: [
                {name: "Acquestscript", size: "56", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/22.png`},
                {name: "AnastasiaScript", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/23.png`},
                {name: "Aquarelle", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/24.png`},
                {name: "Aurora Script", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/25.png`},
                {name: "Bolero Script", size: "45", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/28.png`},
                {name: "Boyarsky", size: "40", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/29.png`},
                {name: "Copyist Thin", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/30.png`},
                {name: "Corinthia", size: "60", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/31.png`},
                {name: "CorridaC", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/33.png`},
                {name: "EnglishScript", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/34.png`},
                {name: "Esenin Script One", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/35.png`},
                {name: "Heinrich Script", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/36.png`},
                {name: "Isabella-Decor", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/37.png`},
                {name: "OlgaCTT", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/38.png`},
                {name: "Pompadur", size: "40", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/39.png`},
                {name: "Round Script", size: "40", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/40.png`},
                {name: "StudioScriptCTT", size: "50", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/42.png`},
                {name: "Venecia", size: "45", icon: `${process.env.PUBLIC_URL}/static/images/fonts-button/43.png`}
            ]
        }
    }

    changeLanguage(lang) {
        this.props.onChangeLanguage(lang);
    }

    changeFont(font) {
        this.props.onChangeFont(font);
    }

    addText() {
        this.props.onAddFigure({
            text: this.text.current.value,
            font: this.props.appState.textFont,
            identify: new Date().getTime()
        });
    }

    render() {
        return (
            <div className="AddText">
                <div className="Figures__title">
                    Добавьте подвеску с именем
                </div>

                <div className="AddText-language">
                    <button className={
                        classNames(
                            'AddText-language__item',
                            {'AddText-language__item--active': this.props.appState.textLanguage === 'ru'}
                        )
                    } onClick={() => this.changeLanguage('ru')}>Русские буквы</button>
                    <button className={
                        classNames(
                            'AddText-language__item',
                            {'AddText-language__item--active': this.props.appState.textLanguage === 'en'}
                        )
                    } onClick={() => this.changeLanguage('en')}>Английские буквы</button>
                </div>

                <div className="AddText-list">
                    {this.state[this.props.appState.textLanguage].map(item => (
                        <div className={
                            classNames(
                                'AddText-list__item',
                                {'AddText-list__item--active': this.props.appState.textFont === item.name}
                            )
                        }
                        key={item.name}
                        onClick={() => this.changeFont(item.name)}>
                            <img src={item.icon} alt=""/>
                        </div>
                    ))}
                </div>

                <div className="AddText-input">
                    <input type="text" maxLength="10" ref={ this.text } style={{fontFamily: this.props.appState.textFont}} />
                </div>

                <button className="AddText-add" onClick={() => this.addText()}>Добавить</button>

            </div>
        )
    }
}

export default AddText;