import { connect } from 'react-redux';
import AddText from './AddText';

import { changeLanguage, changeFont, addFigure } from '../../actions/appState';

const mapStateToProps = state => ({
    appState: state.appState
});

const mapDispatchToProps = dispatch => ({
    onChangeLanguage: lang => {
        dispatch(changeLanguage(lang));
    },
    onChangeFont: font => {
        dispatch(changeFont(font));
    },
    onAddFigure: figure => {
        dispatch(addFigure(figure));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AddText)