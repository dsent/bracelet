import { connect } from 'react-redux';
import AddFigure from './AddFigure';

import { addFigure } from '../../actions/appState';

const mapStateToProps = state => ({
    appState: state.appState
});

const mapDispatchToProps = dispatch => ({
    onAddFigure: (figure, category) => {
        dispatch(addFigure(figure, category))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(AddFigure)