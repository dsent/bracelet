import React from 'react';
import './Welcome.css';
import logo from './images/logo.png';

const Welcome = () => {
    return (
        <div className="Welcome">
            <img src={logo} alt=""/>

            <div className="Welcome__text">
                Приветствуем вас в нашем Конструкторе браслетов <br />
                Для оформления дизайна воспользуйтесь меню слева

                <div className="Welcome__text-underline">Выберите цепочку, которую вы хотите добавить на браслет</div>
            </div>
        </div>
    )
};

export default Welcome;