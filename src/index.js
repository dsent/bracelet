import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import App from './components/App';
import FuckOf from './components/FuckOf';
import reducer from './reducers';

import './styles.css';

const middleware = [thunk];
const store = createStore(reducer, composeWithDevTools(
    applyMiddleware(...middleware)
));

function applicationRender() {
    if(window.location.host.indexOf('namenecklace') > -1 || process.env.NODE_ENV === 'development') {
        return <App />;
    } else {
        return <FuckOf />
    }
}

ReactDOM.render(
    <Provider store={store}>
        {applicationRender()}
    </Provider>
, document.getElementById('root'));
