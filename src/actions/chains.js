import axios from 'axios';

export const getChains = (metal) => dispatch => {
    axios
        .get(process.env.REACT_APP_CHAINS_URL)
        .then(response => {
            dispatch({ type: 'FETCH_CHAINS', payload: response.data[metal] })
        })
        .catch(error => {
            console.log(error);
        });
};
