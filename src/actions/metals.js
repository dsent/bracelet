const metalsData = [
    {
        id: 1,
        code: "silver",
        name: "Серебро",
        color: "#9e9d9d"
    },
    {
        id: 2,
        code: "gold",
        name: "Желтое золото",
        color: "#DCC58D"
    },
    {
        id: 3,
        code: "wgold",
        name: "Белое золото",
        color: "#9e9d9d"
    },
    {
        id: 4,
        code: "rgold",
        name: "Красное золото",
        color: "#e5b690"
    }
];

export const getMetals = () => dispatch => {
    setTimeout(() => {
        dispatch({ type: 'FETCH_METALS', payload: metalsData })
    }, 0)
};
