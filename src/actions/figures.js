import axios from 'axios';

export const getFigures = (metal, type) => dispatch => {
    axios
        .get(process.env.REACT_APP_FIGURES_URL)
        .then(response => {
            dispatch({ type: 'FETCH_FIGURES', payload: response.data[metal][type] || [] })
        })
        .catch(error => {
            console.log(error);
        });
};
