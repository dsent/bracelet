export const changeMetal = (metalCode) => dispatch => {
    dispatch({ type: 'CHANGE_METALS', payload: metalCode });
};

export const changeChain = (chainCode) => dispatch => {
    dispatch({ type: 'CHANGE_CHAINS', payload: chainCode })
};

export const changeAddType = (type) => dispatch => {
    dispatch({ type: 'CHANGE_ADD_TYPE', payload: type })
};

export const selectFigure = (figure) => dispatch => {
    dispatch({ type: 'SELECT_FIGURE', payload: figure })
};

export const addFigure = (figure, category) => dispatch => {
    dispatch({ type: 'ADD_FIGURE', payload: { figure, category } })
};

export const openEditFigure = (type, figure) => dispatch => {
    dispatch({ type: 'OPEN_EDIT_FIGURE', payload: { type, figure } })
};

export const changeFiguresList = (figuresList) => dispatch => {
    dispatch({ type: 'CHANGE_FIGURES_LIST', payload: figuresList })
};

export const editFigure = (figuresList) => dispatch => {
    dispatch({ type: 'EDIT_FIGURE', payload: figuresList })
};

export const removeFigure = (figuresList) => dispatch => {
    dispatch({ type: 'REMOVE_FIGURE', payload: figuresList })
};

export const changeLanguage = (lang) => dispatch => {
    dispatch({ type: 'CHANGE_LANGUAGE', payload: lang })
};

export const changeFont = (font) => dispatch => {
    dispatch({ type: 'CHANGE_FONT', payload: font })
};

export const changeRhodium = () => dispatch => {
    dispatch({ type: 'CHANGE_RHODIUM' })
};

export const changeGilding = () => dispatch => {
    dispatch({ type: 'CHANGE_GILDING' })
};