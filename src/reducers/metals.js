const initialState = [];

export default function metals(state = initialState, action) {
    if (action.type === 'FETCH_METALS') {
        return action.payload;
    }
    return state;
}
