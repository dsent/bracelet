const initialState = [];

export default function figures(state = initialState, action) {
    if (action.type === 'FETCH_FIGURES') {
        return action.payload;
    }
    return state;
}
