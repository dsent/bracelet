const initialState = [];

export default function chains(state = initialState, action) {
    if (action.type === 'FETCH_CHAINS') {
        return action.payload;
    }
    return state;
}
