const initialState = {
    metal: 'silver',
    chain: '',
    rhodium: false,
    gilding: false,
    braceletItems: [],
    textLanguage: 'ru',
    textFont: 'Acquestscript',

    selectAddType: '',
    figureViewing: null
};

export default function appState(state = initialState, action) {
    switch (action.type) {
        case 'CHANGE_METALS':
            return {
                ...state,
                metal: action.payload,
                braceletItems: [],
                chain: '',
                figureViewing: null,
                rhodium: false,
                gilding: false
            };

        case 'CHANGE_CHAINS':
            return {
                ...state,
                chain: action.payload
            };

        case 'CHANGE_ADD_TYPE':
            return {
                ...state,
                selectAddType: action.payload,
                figureViewing: null
            };

        case 'SELECT_FIGURE':
            return {
                ...state,
                figureViewing: action.payload
            };

        case 'ADD_FIGURE':
            return {
                ...state,
                selectAddType: action.payload.category,
                figureViewing: null,
                braceletItems: [
                    ...state.braceletItems,
                    action.payload.figure
                ]
            };

        case 'OPEN_EDIT_FIGURE':
            return {
                ...state,
                selectAddType: action.payload.type,
                figureViewing: action.payload.figure
            };

        case 'CHANGE_FIGURES_LIST':
            return {
                ...state,
                braceletItems: action.payload
            };

        case 'REMOVE_FIGURE':
            return {
                ...state,
                selectAddType: '',
                figureViewing: null,
                braceletItems: action.payload
            };

        case 'EDIT_FIGURE':
            return {
                ...state,
                selectAddType: '',
                figureViewing: null,
                braceletItems: action.payload
            };

        case 'CHANGE_LANGUAGE':
            return {
                ...state,
                textLanguage: action.payload
            };

        case 'CHANGE_FONT':
            return {
                ...state,
                textFont: action.payload
            };

        case 'CHANGE_RHODIUM':
            return {
                ...state,
                rhodium: !state.rhodium
            };

        case 'CHANGE_GILDING':
            return {
                ...state,
                gilding: !state.gilding
            };

        default:
            return state;
    }
}
