import { combineReducers } from 'redux';

import appState from './appState';
import metals from './metals';
import chains from './chains';
import figures from './figures';

export default combineReducers({
    appState,
    metals,
    chains,
    figures
})